# Introduction


The idea is to manage deployments by using Git for the desired state. The infrastructure and application should be version controlled. When upgrading, deploying, scaling the application and the infrastructure this should be committed to Git and then it will be applied. This also means that it is possible to roll back to previous commit if some issues are discovered. Getting difference between different deployments can also easily be found.

Here the deployment of application is described in detail. GitOps for infrastructure is similar.

For this to work both the product and the CPD infrastructure needs to be released as versions. COPS that are responsible for each environment (the Git repository that holds the desired state of the environments) will use these versions. If they need special tuning like scaling of pods, memory and CPU settings, ports to be open, whitelisting of IP addresses, special monitoring etc. then they can customize this in the Git repository without having to order new versions of product or infrastructure.

Even if the infrastructure and the application can be controlled by GitOps, the database cannot.

# Deployment of application

For Kubernetes there are to popular tools.

* ArgoCD https://argoproj.github.io/argo-cd/
* Flux v2 https://fluxcd.io/

ArgoCD seems to be the most mature at the moment.

When committing everything to git there is always a question about credentials and security. This is addressed in the security section.

Reference: https://cloudogu.com/en/blog/gitops-tools

## Main directory structure

The Git repository is built up by two main parts:

* versions: containing the deployment scripts delivered by product.
* deployments: containing deployments for specific clusters and applications (Traefik, cluster roles etc.).

The tool Kustomize is used to have layered deployment manifests.

The deployments for clusters and applications refers to product versions. Hence, by updating reference to a newer version the application will be upgraded.

```
├── deployments
│   ├── apps
│   │   ├── ec-1
│   │   │   ├── kustomization.yaml
│   │   │   └── ...
│   │   └── ec-2
│   │       ├── app
│   │       │   ├── kustomization.yaml
│   │       │   └── ...
│   │       └── db
│   │           ├── kustomization.yaml
│   │           └── ...
│   └── clusters
│       └── olberg.homelinux.com
│           ├── infra
│           │   └── ...
│           └── k8s
│               └── kustomization.yaml
└── versions
    ├── 13.0.6
    │   └── ...
    ├── 13.0.7
    │   └── ...
    └── 13.0.8
        └── ...

```

The versions directory contains the deployment files from EC and CPD releases. When there is a new release then this can either be added to this directory automatically by a build pipeline or done manually.

The deployments directory contains the actual deployments. Above there is one directory apps containing two EC deployments, ec-1 and ec-2. The clusters directory contains deployment of cluster wide deployments. Currently this is deployment of the Kubernetes ingress controller Traefik.

### Version directory for a release

```
└── versions
└── versions
    ├── 13.0.12
    │   ├── kubernetes-deployment-app
    │   │   ├── ec-app
    │   │   │   ├── ec-app.properties
    │   │   │   └── ec-app.yaml
    │   │   ├── ec-bpm
    │   │   │   ├── ec-ecbpm.properties
    │   │   │   └── ec-ecbpm.yaml
    │   │   ├── ec-ingress
    │   │   │   └── ec-ingress.yaml
    │   │   ├── ec-keycloak
    │   │   │   ├── keycloak.properties
    │   │   │   └── keycloak.yaml
    │   │   ├── ec-ra
    │   │   │   ├── ec-ra.properties
    │   │   │   └── ec-ra.yaml
    │   │   ├── kustomization.yaml
    │   │   ├── rbac
    │   │   │   └── kubeping-rbac.yaml
    │   │   └── vault
    │   │       ├── ec-vault-config.yaml
    │   │       └── ec-vault-data.yaml
    │   ├── kubernetes-deployment-cluster
    │   │   ├── ingressctrl
    │   │   │   ├── ingress-ping.yaml
    │   │   │   └── traefik.yaml
    │   │   ├── kustomization.yaml
    │   │   └── volumes
    │   │       └── pv-extensions.yaml
    │   └── kubernetes-deployment-db
    │       ├── db.yaml
    │       └── kustomization.yaml
    ├── 13.1.0
    │   ├── kubernetes-deployment-app
    │   │   ├── ec-app
    │   │   │   ├── ec-app.properties
    │   │   │   └── ec-app.yaml
    │   │   ├── ec-ecbpm
    │   │   │   ├── ec-ecbpm.properties
    │   │   │   └── ec-ecbpm.yaml
    │   │   ├── ec-ingress
    │   │   │   └── ec-ingress.yaml
    │   │   ├── ec-keycloak
    │   │   │   ├── ec-keycloak.properties
    │   │   │   └── ec-keycloak.yaml
    │   │   ├── ec-ra
    │   │   │   ├── ec-ra.properties
    │   │   │   └── ec-ra.yaml
    │   │   ├── kustomization.yaml
    │   │   └── rbac
    │   │       └── kubeping-rbac.yaml
    │   ├── kubernetes-deployment-cluster
    │   │   ├── ingressctrl
    │   │   │   ├── ingress-ping.yaml
    │   │   │   └── traefik.yaml
    │   │   └── kustomization.yaml
    │   └── kubernetes-deployment-db
    │       ├── db.yaml
    │       └── kustomization.yaml
    ├── 13.1.1

```

The deployment for an EC release are devided into several directories:

* app - for deployment of the application
* cluster - for deployment of the ingress controller Traefik
* db - for deployment of the test database (Not used by real environments)

The deployments scripts for a release cannot contain real URLs. For those it should contain place holders so that it is obvious that these need to get real values for actual deployments.

Example from the file versions/13.0.6/app/ec-app/ec-app.properties:

```
#
# The full list of environment variables can be found at:
#
#      documentation/src/docs/modules/technical-documentation/pages/containers/ec-ec-app.adoc
#
# Should be false outside maintenance window
DB_MIGRATE_ON_BOOT=false
ALLOW_INCOMPATIBLE_DB=false
# DEBUG
ENABLE_DEBUGGING=false
EC_LOGLEVEL=INFO
# Database settings
DB_HOSTNAME=db
DB_PORT=1521
DB_SERVICENAME=orcl
DB_USERNAME=energyx_${EC_OPERTION}
DB_PASSWORD=energy
DB_ADMINUSER=eckernel_${EC_OPERTION}
DB_ADMINPASSWORD=energy
MGMT_USER=admin
MGMT_PASSWORD=admin
EC_URL_APP=http://ec-app-service:8080
EC_URL_AUTH_SERVER=https://${NAMESPACE}.${DOMAIN}/auth
EC_URL_BPM=http://ec-ecbpm-service:8080/jbpm-console
EC_URL_RA=http://yellowfin-service:8080/ra
EC_URL_RA_EXT=https://${NAMESPACE}.${DOMAIN}/ra
PROXY_ADDRESS_FORWARDING=true
ECRA_ADMIN_USERNAME=admin
ECRA_ADMIN_PASSWORD=admin
ECWORKER_CLIENT_SECRET=CHANGE_ME
BPM_CLIENT_SECRET=CHANGE_ME
EC_JGROUPS_STACK=kubernetes
EC_KUBE_PING_PROPERTIES=namespace="${NAMESPACE}",labels="app=ec-app",port_range=0,masterHost=kubernetes.default.svc,masterPort=443
WILDFLY_STATISTICS=true
#EC_MARK_HTTP_SECURE=true
EC_SESSION_COOKIE_SECURE=true
```
Here it is shown that some URLs are real ones pointing to a service local to the kubernetes namespace and some values have place holders that will not work unless replaced for actual deployments.

### Deployment directory containing actual deployments

```
├── deployments
│   ├── apps
│   │   ├── ec-1
│   │   │   ├── config-maps.yaml
│   │   │   ├── dockerconfig.yaml
│   │   │   ├── ec-vault-sealed.yaml
│   │   │   ├── ec-vaults.yaml
│   │   │   ├── kustomization.yaml
│   │   │   ├── namespace.yaml
│   │   │   ├── patch-ingress.json
│   │   │   ├── replicas.yaml
│   │   │   └── resources.yaml
│   │   └── ...
│   └── clusters
│       └── olberg.homelinux.com
│           └── kustomization.yaml
```

Here there is one deployment for the application ec-1 which is deployed to a namespace ec-1 and a cluster ingress controller deployed to a cluster named olberg.homelinux.com.

When creating applications in ArgoCD then the path set for ec-1 will be deployments/apps/ec-1 and for the cluster it will be deployments/clusters/olberg.homelinux.com

When ArgoCD syncronizes the deployment of ec-1 it will run kustomize in the directory deployments/apps/ec-1 and compare that with what is currently deployed.

The main file is the kustomization.yaml.

```
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
namespace: ec-1
#namePrefix:
bases:
- namespace.yaml
- dockerconfig.yaml
- ../../../versions/13.0.8/db
- ../../../versions/13.0.8/app

patchesStrategicMerge:
- replicas.yaml
- resources.yaml
- config-maps.yaml

resources:
- ec-vaults.yaml

patches:
- target:
    kind: Ingress
    name: ec-ingress
  path: patch-ingress.json
```

Here we can see that:

* the application will deployed in a namespace ec-1.
* the test database of version 13.0.8 will be deployed
* the application of version 13.0.8 will be deployed
* the replicas, resources and config maps will be patched for the ec-1 deployment.
* the vault for ec-1 deployment will be loaded
* the ingress for the application will be patched to get the correct URLs for the ec-1 application

To upgrade a deployment the only thing that needs to be done is to update the reference to the version in the kustomization.yaml file. This is valid as long as the properties from the release do not change names and there are not introduced new properties that needs to be customized.

The syntax and test deployments can be done by running the following commands in the directory deployments/apps/ec-1

* kustomize build
* kustomize build | kubectl apply -f -


Deployment of the Traefik ingress controller is simpler since it does not have any place holders.

The file deployments/clusters/olberg.homelinux.com/kustomization.yaml:
```
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
bases:
- ../../../versions/13.0.7/cluster
```

Here it tells that the cluster has ingress controller shipped by release 13.0.7. To update the cluster to use version 13.0.8 the only change needed is to change the reference in the kustomization.yaml file.

## CPD releasing versions of infrastructure

CPD delivers the infrastructure and also software components like datadog agents etc. This should be tagged and released so that it is possible to know exactly what version of the infrastructure a given environment has. If datadog agent is part of namespace it should be added to the application kustomization.yaml. If it is for the cluster it should be added to the kustomization.yaml of the cluster. These deployments then refers to what version of CPD code to deploy. Then COPS can decide what to use and where to use datadog prometheus etc.

The infrastructure itself should also me managed in git and not by manually running Jenkins jobs. Update git and that will have what is needed for the jenkins job.

# Security

Using GitOps always brings one qyestion to the table: What about secrets? Since the full state should be in Git there is a problem because secrets cannot be committed to git without exposing them.

Another problem with Kubernetes is that what is called secrets are actually not. It is simply a ConfigMap where the values are base64 encoded. It is straight forward to decrypt them so they cannot be committed to git and should also be restricted with RBAC in kubernetes so only the ones allowed to se the secrets can see them.

Bitnami has a solution for this. They have introduced sealed secrets to Kubernetes. The sealed secrets are secrets that are encrypted by the cluster. The encrypted secrets can be checked into git. However, only the intended cluster can then decrypt it and if the if cluster is recreated then there will be new encryption keys and the secrets in git will be useless. This can be overcome by setting the encryption key to a fixed key but where to store this?

More information about sealed secrets can be found at:

* https://github.com/bitnami-labs/sealed-secrets

Another approach is to use AWS Secrets. Then AWS will manage the secrets and the properties will get the values from AWS. This is what DaWinci does.

More information about AWS Secrets can be found at:

* https://docs.aws.amazon.com/secretsmanager/latest/userguide/integrating_csi_driver.html


The containers also needs to be secured so that it is not possible to dump the environment variables. The /bin/env has to be removed from the containers.

Using vaults as it has been done so far is not so good for GitOps. It is complex to make the vault and by committing the secrets to git will expose them and the vault can be compromised.

Normal users accessing the cluster should can be restricted so they do not see secrets. What kind of roles normal users should have has to be defined.

Database migration is today turned on as part of deployment. Should this be a setting in the db and not part of deployment to avoid a mistake start database migration?


# References
* Sealed secrets introduction https://www.youtube.com/watch?v=ShGHCpUMdOg&ab_channel=AntonPutra
* Flux https://www.youtube.com/watch?v=X9R5ySkiUkc&ab_channel=AntonPutra
* ArgoCD on EKS: https://www.eksworkshop.com/intermediate/290_argocd/
* AWS secrets https://www.youtube.com/watch?v=Rmgo6vCytsg&ab_channel=AntonPutra
