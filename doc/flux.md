# Introduction

## Installation

### Terraform

- https://registry.terraform.io/modules/kube-champ/flux-bootstrap/k8s/latest

### By using the tool flux

Bootstrap flux:
```
flux bootstrap git \
  --url=ssh://git@bitbucket.org/akhauabh/ecsm-ec-abhi-nonprod-ops.git \
  --branch=production \
  --path=cluster/common \
  --private-key-file=/home/eol/.ssh/gitops-test \
  --kubeconfig $KUBECONFIG
```

Create git source from scratch

```

flux create secret git ec-repo-auth \
    --url=ssh://git@bitbucket.org/akhauabh/ecsm-ec-abhi-nonprod-ops.git \
    --private-key-file=~/.ssh/gitops-test

flux create secret git flux-system \
    --url=ssh://git@bitbucket.org/akhauabh/ecsm-ec-abhi-nonprod-ops.git \
    --private-key-file=~/.ssh/gitops-test

flux create source git ec-repo \
    --url=ssh://git@bitbucket.org/akhauabh/ecsm-ec-abhi-nonprod-ops.git \
    --branch=production     \
    --secret-ref ec-repo-auth\
    --export > ec-repo.yaml


```

Create kustomisations per application

```
# Run commands in cluster/common/flux-system

# Create a Kustomization resource for deployment of ec-dev

flux create kustomization ec-dev \
  --source=GitRepository/ec-repo \
  --path="./cluster/apps/ec-dev" \
  --prune=true \
  --interval=5m \
  --export > ec-dev-sync.yaml

# Create a Kustomization resource for deployment of ec-dev

flux create kustomization ec-test \
  --source=GitRepository/flux-system \
  --path="./cluster/apps/ec-test" \
  --prune=true \
  --interval=5m \
  --export > ec-test-sync.yaml

```

# Add Teams notifications

```
kubectl -n flux-system create secret generic msteams-url \
--from-literal=address=https://qbsol.webhook.office.com/webhookb2/984ef36f-a0e4-4d3e-8f0a-da28d4ab925c@ce68f836-c221-45ef-866b-38cda86b3d5e/IncomingWebhook/d5c96e7fda744a0da53d6a5503143910/c56efa3f-067b-42e5-8fdc-f1e2b8f54502

cat <<EOF | kubectl apply -f -
apiVersion: notification.toolkit.fluxcd.io/v1beta1
kind: Provider
metadata:
  name: msteams
  namespace: flux-system
spec:
  type: msteams
  channel: general
  secretRef:
    name: msteams-url
---
apiVersion: notification.toolkit.fluxcd.io/v1beta1
kind: Alert
metadata:
  name: on-call-msteams
  namespace: flux-system
spec:
  providerRef:
    name: msteams
  eventSeverity: info
  eventSources:
    - kind: GitRepository
      name: '*'
    - kind: Kustomization
      name: '*'
---
EOF

```


## References

- https://fluxcd.io/docs/
- https://fluxcd.io/docs/use-cases/aws-codecommit/
- https://fluxcd.io/docs/guides/repository-structure/
-
