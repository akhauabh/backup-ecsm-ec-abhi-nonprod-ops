# Process for onboarding a new project

## One gitops repository per AWS Account vs many in one

In gitops will the ones commiting code be like system operators.
If it is essential to have separate access to the different environments then these environments must not be in the same repository.


## Responibilities for tools like ENV0

- On boarding of a new account (bootsrapping)
  - Have some few parameters
  - Create gitops repository
  - Create database
  - Create gitops repository/add to gitops repository
- Maintaining environments in gitops repositories
  - Creating destroying of on boarded environments
  - Scheduling create destroy to save
- Rotating credentials
- Upgrading environments
- Copy from prod to test/dev
  - hooks for updating credentials, cleanup etc.
