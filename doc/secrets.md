# Secrets

There are several different secrets. Some examples are credentials to databases and git repositories.

These can be stored in git repository or in AWS secret store.

Some alternatives to use are:

- sops + AWS KMS
- bitnami sealed secrets
- AWS Secrets Manager


We need to think of process when rotating secrets for database passwords. It would be best to set new password in one place.

Basically there are two different approaches:

1. Keep the secrets in an external system like Hashicorp Vault, Azure Key Vault or AWS SecretsManager and find a way to inject them at runtime. Preferably the agent running the app has some rights based on identity for this. This means we need to setup identity management as well as a vault to keep the secrets. This adds overhead and some risks because it is not always directly clear what the potential risks are. But the advantage is that we can rotate secrets easily, and also by identity management assign or revoke access as the situation demands.
2. Keep the secrets encrypted in git. For this we need to manage access to the encryption key(s). The obvious advantage is that we do not need an external vault, and this may drive development speed and reduces complexity somewhat on that end. The disadvantage is that the deployment secrets are now coupled to the code in git, and in case secrets need to be rotated it involves a new deployment. If you have some gitops and automated pipelines in place this might not be a big problem, but in some situations this is a more serious issue. It also means that git bisect will no longer reliably return an answer.


## SOPS & AWS KMS

- https://www.youtube.com/watch?v=V2PRhxphH2w&ab_channel=SecuringDevOps
- https://github.com/mozilla/sops
- https://calzone.proofofpizza.com/tech/tutorial/using-sops-with-aws-and-terraform/
- https://medium.com/mercos-engineering/secrets-as-a-code-with-mozilla-sops-and-aws-kms-d069c45ae1b9
- https://gist.github.com/davidcallen/36e1d068fc352f4563297c7eb96f287d
- https://github.com/mozilla/sops/tree/0494bc41911bc6e050ddd8a5da2bbb071a79a5b7#up-and-running-in-60-seconds



## Bitnami Sealed Secrets

- https://github.com/bitnami-labs/sealed-secrets
- https://fluxcd.io/docs/guides/sealed-secrets/



## AWS Secrets Manager

The AWS Secrets Manager will make it possible to automate password rotation and synchronize database password and application. 

- https://docs.aws.amazon.com/eks/latest/userguide/manage-secrets.html
- https://aws.amazon.com/blogs/security/rotate-amazon-rds-database-credentials-automatically-with-aws-secrets-manager/
- https://aws.amazon.com/blogs/containers/aws-secrets-manager-controller-poc-an-eks-operator-for-automatic-rotation-of-secrets/
