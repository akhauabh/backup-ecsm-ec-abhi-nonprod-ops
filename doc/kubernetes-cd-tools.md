# Kubernetes continuous deployment tools

## Flux
- no users
- no graphical UI
- Distributed

## ArgoCD
- Distributed and central
- Need to configure users and access
- graphical interface
