#!/bin/bash

# Environment variables

EC_CUSTOMER=egil
EC_PROJECT=olberg
EC_SYSTEM=non-prod
SSH_PRIVATE_KEY=~/.ssh/gitops-test

GIT_REPOSITORY=ssh://git@bitbucket.org/energycomponents/ec-gitops-test.git


# Teeform flux (flux intall)
# Generate flux-system
( 
  cd flux-system 
  flux install --export > gotk-components.yaml 
  kubectl apply -f gotk-components.yaml
  flux check

  # Generate source
  flux create source git flux-system \
    --namespace=flux-system \
    --git-implementation=libgit2 \
    --url=${GIT_REPOSITORY} \
    --branch=master \
    --private-key-file=${SSH_PRIVATE_KEY} \
    --interval=1m

   flux export source git flux-system \
     > gotk-sync.yaml

   git add -A && git commit -m "add flux-system components" && git push

   flux create kustomization flux-system \
    --source=flux-system \
    --path="./deployments/${EC_CUSTOMER}/${EC_PROJECT}/${EC_SYSTEM}/common" \
    --prune=true \
    --interval=10m

    flux export kustomization flux-system \
      >> gotk-sync.yaml
   
)


