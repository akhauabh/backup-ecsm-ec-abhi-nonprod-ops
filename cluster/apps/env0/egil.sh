#!/bin/bash

$(aws ecr get-login | sed -e "s/-e none//")

for imagename in nexus.ecdevops.net:5555/docker-flyway-db-testdata:13.1.0 nexus.ecdevops.net:5555/ec-ec-app:13.1.0 nexus.ecdevops.net:5555/ec-ecbpm:13.1.0 nexus.ecdevops.net:5555/ec-keycloak:13.1.0 nexus.ecdevops.net:5555/ec-ra:13.1.0
do

    echo "Processing iname name: $imagename"
    DOCKER_IMAGE="$(echo "$imagename" |awk -F '/' '{print $2}')"
    DOCKER_IMAGE_NAME="$(echo "$DOCKER_IMAGE" |awk -F ':' '{print $1}')"
    DOCKER_IMAGE_VERSION="$(echo "$DOCKER_IMAGE" |awk -F ':' '{print $2}')"

    ECR_IMAGE="832620600113.dkr.ecr.eu-west-1.amazonaws.com/$DOCKER_IMAGE"

    echo "Upload docker image $DOCKER_IMAGE_NAME:$DOCKER_IMAGE_VERSION to ECR"

    docker pull $imagename
    set +e
    aws ecr create-repository --repository-name $DOCKER_IMAGE_NAME
    set -e
    docker tag $imagename $ECR_IMAGE
    docker push $ECR_IMAGE

done

