terraform {
  backend "s3" {
    bucket = "832620600113-dawinci-abhi-s3-tfstate"
    key    = "eu-west-1/dawinci/env0/EKS-Cluster/terraform.tfstate"
    region = "eu-west-1"
  }
}

provider "aws" {
  # access_key = var.aws_access_key
  # secret_key = var.aws_secret_key
  region = var.aws_region
  # token      = "${var.token != "" ? var.token : null}"
}

# Fetching Account Details

data "aws_caller_identity" "account_details" {
}

# Fetching SSM parameter account_name

data "aws_ssm_parameter" "account_name" {
  name = "${var.product}-${var.customer_name}-account_name"
}

# Fetching SSM parameter organization

data "aws_ssm_parameter" "organization" {
  name = "${var.product}-${var.customer_name}-organization"
}

# Fetching SSM parameter terpcode

data "aws_ssm_parameter" "terpcode" {
  name = "${var.product}-${var.customer_name}-terpcode"
}

locals {
  common_tags = {
    AccountName  = data.aws_ssm_parameter.account_name.value
    CustomerName = var.customer_name
    Environment  = var.env_name
    Terpcode     = data.aws_ssm_parameter.terpcode.value
    # CreatedBy    = data.aws_caller_identity.account_details.user_id
  }
}

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket = "832620600113-dawinci-abhi-s3-tfstate"
    key    = "env:/${data.aws_caller_identity.account_details.account_id}/${var.aws_region}/${var.product}/${var.network_env}/Network/terraform.tfstate"
    region = var.aws_region
  }
}

locals {
  private_subnets = ["${data.terraform_remote_state.vpc.outputs.private_subnet_1_id}", "${data.terraform_remote_state.vpc.outputs.private_subnet_2_id}"]
  public_subnets  = ["${data.terraform_remote_state.vpc.outputs.public_subnet_1_id}", "${data.terraform_remote_state.vpc.outputs.public_subnet_2_id}"]
  all_subnets     = ["${data.terraform_remote_state.vpc.outputs.private_subnet_1_id}", "${data.terraform_remote_state.vpc.outputs.private_subnet_2_id}", "${data.terraform_remote_state.vpc.outputs.public_subnet_1_id}", "${data.terraform_remote_state.vpc.outputs.public_subnet_2_id}"]
}

# IAM Role for EKS Cluster

module "Cluster-IAM-Role" {
  source = "git::ssh://git@bitbucket.org/akhauabh/env0_mod.git/src/master//Modules/IAM/IAM_Roles/EKS-Cluster-Role/?ref=v0.0.30"
  # source = "../../../../Terraform/Modules/IAM/IAM_Roles/EKS-Cluster-Role/"
  name = "${var.product}-${var.customer_name}-${var.env_name}-EKS-Cluster-Role"
  tags = local.common_tags
}

# Additional Security Group for Cluster

resource "aws_security_group" "cluster_sg" {
  name        = "${var.product}-${var.customer_name}-${var.env_name}-EKS-SG"
  vpc_id      = data.terraform_remote_state.vpc.outputs.vpc_id
  description = "Clustersecurity group"
  tags        = local.common_tags
}

resource "aws_security_group_rule" "all_443" {
  type              = "ingress"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "443 access from internet"
  security_group_id = aws_security_group.cluster_sg.id
}

resource "aws_security_group_rule" "cluster_egress_all" {
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = -1
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "egress all"
  security_group_id = aws_security_group.cluster_sg.id
}

# Create EKS Cluster

module "EKS-Cluster" {
  source = "git::ssh://git@bitbucket.org/akhauabh/env0_mod.git/src/master//Modules/Containers/EKS/Cluster/?ref=v0.0.30"
  # source = "../../../../Terraform/Modules/Containers/EKS/Cluster/"
  name                                 = "${var.product}-${var.customer_name}-${var.env_name}-Cluster"
  role_arn                             = module.Cluster-IAM-Role.cluster_role_arn
  eks_version                          = var.eks_version
  log_type                             = var.enable_cluster_logs == "true" ? ["api", "audit", "authenticator", "controllerManager", "scheduler"] : null
  logs_enabled                         = var.enable_cluster_logs
  cluster_security_group_id            = [aws_security_group.cluster_sg.id]
  subnets                              = local.all_subnets
  cluster_endpoint_private_access      = var.cluster_endpoint_private_access
  cluster_endpoint_public_access       = var.cluster_endpoint_public_access
  cluster_endpoint_public_access_cidrs = "0.0.0.0/0"
  tags                                 = local.common_tags
}


# IAM Role for Node Group 

module "Node-IAM-Role" {
  source = "git::ssh://git@bitbucket.org/akhauabh/env0_mod.git/src/master//Modules/IAM/IAM_Roles/EKS-Node-Group-Role/?ref=v0.0.30"
  # source = "../../../../Terraform/Modules/IAM/IAM_Roles/EKS-Node-Group-Role/"
  name               = "${var.product}-${var.customer_name}-${var.env_name}-EKS-NG-Role"
  region             = var.aws_region
  account_id         = data.aws_caller_identity.account_details.account_id
  artefacts_bucket   = "${data.aws_caller_identity.account_details.account_id}-${var.product}-${var.customer_name}-s3-artefacts"
  sftp_bucket        = "${data.aws_caller_identity.account_details.account_id}-${var.product}-${var.customer_name}-${var.env_name}-s3-sftp"
  access_logs_bucket = "${data.aws_caller_identity.account_details.account_id}-${var.product}-${var.customer_name}-s3-access-logs"
  organization       = data.aws_ssm_parameter.organization.value
  tags               = local.common_tags
}

# Create Node Groups for EKS

module "Node-Group" {
  source = "git::ssh://git@bitbucket.org/akhauabh/env0_mod.git/src/master//Modules/Containers/EKS/Node-Group/?ref=v0.0.30"
  # source = "../../../../Terraform/Modules/Containers/EKS/Node-Group/"
  cluster_name    = module.EKS-Cluster.cluster_id
  ng_name         = "${var.product}-${var.customer_name}-${var.env_name}-Node-Group"
  node_role_arn   = module.Node-IAM-Role.ng_role_arn
  subnets         = var.subnets == "private" ? local.private_subnets : local.public_subnets
  desired_size    = var.desired_size
  max_size        = var.max_size
  min_size        = var.min_size
  ami_type        = "AL2_x86_64"
  disk_size       = var.disk_size
  instance_types  = [var.instance_type]
  release_version = ""
  tags            = local.common_tags
  # ec2_ssh_key = var.ec2_ssh_key
}

data "aws_eks_cluster_auth" "auth" {
  name = "${var.product}-${var.customer_name}-${var.env_name}-Cluster"
}




module "kubeconfig" {
  depends_on = [module.EKS-Cluster]
  source     = "git::ssh://git@bitbucket.org/akhauabh/env0_mod.git/src/master//Modules/Containers/EKS/Kubeconfig-Backup/?ref=v0.0.30"
  # source = "../../../../Terraform/Modules/Containers/EKS/Node-Group/"
  product       = var.product
  customer_name = var.customer_name
  env_name      = var.env_name
  kms_key       = var.kms_key
  tags          = local.common_tags
  region        = var.aws_region
  # ec2_ssh_key = var.ec2_ssh_key
}


# Create Kubernetes policy

resource "aws_iam_policy" "kubernetes-policy" {
  count       = var.kubernetes_policy == "true" ? 1 : 0
  name        = "${var.product}-${var.customer_name}-${var.env_name}-kubernetes"
  description = "Kubernetes policy"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "getallsecrets",
            "Effect": "Allow",
            "Action": [
                "secretsmanager:GetResourcePolicy",
                "secretsmanager:GetSecretValue",
                "secretsmanager:DescribeSecret",
                "secretsmanager:ListSecretVersionIds"
            ],
            "Resource": "*"
        },
        {
            "Sid": "secretslists",
            "Effect": "Allow",
            "Action": [
                "secretsmanager:GetRandomPassword",
                "secretsmanager:ListSecrets"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}