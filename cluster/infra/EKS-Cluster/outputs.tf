output "Cluster_ID" {
  value = module.EKS-Cluster.cluster_id
}

output "Cluster_arn" {
  value = module.EKS-Cluster.cluster_arn
}

output "Cluster_endpoint" {
  value = module.EKS-Cluster.cluster_endpoint
}

output "Cluster_Version" {
  value = module.EKS-Cluster.cluster_version
}

output "Cluster_Role_arn" {
  value = module.Cluster-IAM-Role.cluster_role_arn
}

output "Cluster_Role_Name" {
  value = module.Cluster-IAM-Role.cluster_role_name
}

output "Node_Group_Role_Arn" {
  value = module.Node-Group.node_group_arn
}

output "Node_Group_Role_Id" {
  value = module.Node-Group.node_group_id
}

output "Node_Group_Role_Status" {
  value = module.Node-Group.node_group_status
}

output "token" {
  value     = data.aws_eks_cluster_auth.auth.token
  sensitive = true
}