variable "aws_region" { default = "eu-west-1" }

variable "product" { default = "dawinci" }

variable "customer_name" { default = "abhi" }

variable "env_name" { default = "env0" }

variable "vpc_cidr" { default = "172.16.36.0/23" }

variable "public_subnet_cidr_az_1" { default = "172.16.36.0/25" }
variable "public_subnet_cidr_az_2" { default = "172.16.36.128/25" }
variable "private_subnet_cidr_az_1" { default = "172.16.37.0/25" }
variable "private_subnet_cidr_az_2" { default = "172.16.37.128/25" }

variable "allowed_cidr" {
  default = "NULL"
}

variable "hosted_zone" { default = "teamdemo.tieto-og.cloud" }
variable "vpn_gateway" { default = 1 }

variable "sns_infrastructure_admin_endpoint" { default = "abhishek.akhaury@tietoevry.com" }
variable "sns_infrastructure_ops_endpoint" { default = "abhishek.akhaury@tietoevry.com" }
variable "sns_dba_endpoint" { default = "abhishek.akhaury@tietoevry.com" }
