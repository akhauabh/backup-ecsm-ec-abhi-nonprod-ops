terraform {
  backend "s3" {
    # EXAMPLE bucket = "832620600113-${var.product}-${var.customer_name}-s3-tfstate"
    bucket = "832620600113-dawinci-abhi-s3-tfstate"
    # EXAMPLE key    = "/${var.aws_region}/${var.product}/${var.env_name}/Network/terraform.tfstate"
    key    = "eu-west-1/dawinci/env0/Network/terraform.tfstate"
    region = "eu-west-1"
  }
}

provider "aws" {
  # access_key = var.aws_access_key
  # secret_key = var.aws_secret_key
  region = var.aws_region
  # token      = "${var.token != "" ? var.token : null}"
}

# Fetching Account Details

data "aws_caller_identity" "account_details" {
}

# Fetching SSM parameter account_name

data "aws_ssm_parameter" "account_name" {
  name = "${var.product}-${var.customer_name}-account_name"
}

# Fetching SSM parameter terpcode

data "aws_ssm_parameter" "terpcode" {
  name = "${var.product}-${var.customer_name}-terpcode"
}

locals {
  common_tags = {
    AccountName  = data.aws_ssm_parameter.account_name.value
    CustomerName = var.customer_name
    Terpcode     = data.aws_ssm_parameter.terpcode.value
    # CreatedBy    = data.aws_caller_identity.account_details.user_id
  }
}

# VPC Structure

module "vpc_structure" {
  source = "git::ssh://git@bitbucket.org/akhauabh/env0_mod.git/src/master//Modules/Network/VPC/?ref=v0.0.7"
  # source                   = "../../env0_mod/Modules/Network/VPC"
  product                  = var.product
  customer_name            = var.customer_name
  env_name                 = var.env_name
  vpc_cidr                 = var.vpc_cidr
  public_subnet_cidr_az_1  = var.public_subnet_cidr_az_1
  public_subnet_cidr_az_2  = var.public_subnet_cidr_az_2
  private_subnet_cidr_az_1 = var.private_subnet_cidr_az_1
  private_subnet_cidr_az_2 = var.private_subnet_cidr_az_2
  tags                     = local.common_tags
  allowed_cidr             = "NULL"
  vpn_gateway              = 1
  public_subnet_count      = 2
}

module "route_53" {
  source = "git::ssh://git@bitbucket.org/akhauabh/env0_mod.git/src/master//Modules/Network/Route53/Hosted_Zone/?ref=v0.0.7"
  # source      = "../../env0_mod/Modules/Network/Route53/Hosted_Zone/"
  product       = var.product
  customer_name = var.customer_name
  env_name      = var.env_name
  vpc_id        = module.vpc_structure.vpc_id
  hosted_zone   = var.hosted_zone
  tags          = local.common_tags
}

module "sns_topic" {
  source = "git::ssh://git@bitbucket.org/akhauabh/env0_mod.git/src/master//Modules/Management/SNS/Topic/?ref=v0.0.7"
  # source      = "../../../../env0_mod/Modules/Management/SNS/Topic/"
  product                           = var.product
  customer_name                     = var.customer_name
  env_name                          = var.env_name
  aws_region                        = var.aws_region
  sns_infrastructure_admin_endpoint = var.sns_infrastructure_admin_endpoint
  sns_infrastructure_ops_endpoint   = var.sns_infrastructure_ops_endpoint
  sns_dba_endpoint                  = var.sns_dba_endpoint
  tags                              = local.common_tags
}


