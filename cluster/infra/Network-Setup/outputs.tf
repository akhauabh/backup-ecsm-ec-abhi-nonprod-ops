output "vpc_id" {
  value = module.vpc_structure.vpc_id
}

output "vpc_cidr" {
  value = module.vpc_structure.vpc_cidr
}

output "public_subnet_1_cidr" {
  value = module.vpc_structure.public_subnet_1_cidr
}

output "public_subnet_1_id" {
  value = module.vpc_structure.public_subnet_1_id
}

output "public_subnet_2_cidr" {
  value = module.vpc_structure.public_subnet_2_cidr
}

output "public_subnet_2_id" {
  value = module.vpc_structure.public_subnet_2_id
}

output "private_subnet_1_cidr" {
  value = module.vpc_structure.private_subnet_1_cidr
}

output "private_subnet_1_id" {
  value = module.vpc_structure.private_subnet_1_id
}

output "private_subnet_2_cidr" {
  value = module.vpc_structure.private_subnet_2_cidr
}

output "private_subnet_2_id" {
  value = module.vpc_structure.private_subnet_2_id
}

output "public_zone_id" {
  value = module.route_53.public_zone_id
}

output "private_zone_id" {
  value = module.route_53.private_zone_id
}

output "public_hosted_name" {
  value = module.route_53.public_hosted_name
}

output "private_hosted_name" {
  value = module.route_53.private_hosted_name
}